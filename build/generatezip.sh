#!/usr/bin/env bash

rm files.zip
cd ..
zip -r files.zip app.py scripts/
mv files.zip build/
cd build/
