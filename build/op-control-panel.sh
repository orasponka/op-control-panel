#!/usr/bin/env bash

pwd=$(pwd)
cd /usr/share/op-control-panel/
python app.py
cd $pwd
