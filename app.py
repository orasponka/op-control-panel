import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import subprocess

class deleteUserWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Delete User Page")

        self.set_default_size(150,100)
        self.set_border_width(10)
        self.set_resizable(False)

        grid = Gtk.Grid(row_spacing = 5, column_spacing = 5)
        self.add(grid)

        title = Gtk.Label(label="<span size='large'>User Deletion</span>")
        title.set_use_markup(True)

        userLabel = Gtk.Label(label="Username")
        self.user = Gtk.Entry()

        delBtn = Gtk.Button(label="Delete User")
        delBtn.connect("clicked", self.delete_user)

        closeBtn = Gtk.Button(label="Close")
        closeBtn.connect("clicked", self.close)

        grid.attach(title, 0, 0, 1, 1)
        grid.attach(userLabel, 0, 1, 1, 1)
        grid.attach(self.user, 1, 1, 3, 1)
        grid.attach_next_to(delBtn, self.user, Gtk.PositionType.BOTTOM, 3, 1)
        grid.attach_next_to(closeBtn, userLabel, Gtk.PositionType.BOTTOM, 1, 1)

    def close(self, widget):
        self.destroy()
    def delete_user(self, widget):
        user = self.user.get_text()
        subprocess.run(["./scripts/deleteuser", user])

class aurInstallWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="AUR Package Installation")

        self.set_default_size(150, 60)
        self.set_border_width(10)
        self.set_resizable(False)

        grid = Gtk.Grid(row_spacing = 5, column_spacing = 5)
        self.add(grid)

        title = Gtk.Label(label="<span size='large'>AUR Package Installation</span>")
        title.set_use_markup(True)

        urlLabel = Gtk.Label(label="Url Address")
        self.url = Gtk.Entry()
        self.url.set_text("https://aur.archlinux.org/packagename.git")

        termLabel = Gtk.Label(label="Open in terminal")
        self.term = Gtk.CheckButton()

        installBtn = Gtk.Button(label="Install Package")
        installBtn.connect("clicked", self.installpkg)

        closeBtn = Gtk.Button(label="Close")
        closeBtn.connect("clicked", self.close)

        grid.attach(title, 0, 0, 6, 1)
        grid.attach(urlLabel, 0, 1, 1, 1)
        grid.attach(self.url, 2, 1, 50, 1)
        grid.attach(termLabel, 0, 2, 2, 1)
        grid.attach(self.term, 2, 2, 1, 1)
        grid.attach_next_to(closeBtn, self.term, Gtk.PositionType.BOTTOM, 6, 1)
        grid.attach_next_to(installBtn, closeBtn, Gtk.PositionType.RIGHT, 44, 1)

    def installpkg(self, widget):
        url = self.url.get_text()
        if self.term.get_active():
            subprocess.run(["alacritty", "--hold", "-e", "./scripts/installaurpkg", url, "true"])
        else:
            subprocess.run(["./scripts/installaurpkg", url, "false"])
    def close(self, widget):
        self.destroy()

class userAddWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Linux User Creation")

        self.set_default_size(340, 190)
        self.set_border_width(10)
        self.set_resizable(False)

        grid = Gtk.Grid(row_spacing = 10, column_spacing = 10)
        self.add(grid)

        title = Gtk.Label(label="<span size='large'>User Creation</span>")
        title.set_use_markup(True)

        nameLabel = Gtk.Label(label="Username")
        self.name = Gtk.Entry()

        passLabel = Gtk.Label(label="Password")
        self.password = Gtk.Entry()
        self.password.set_visibility(False)

        shellLabel = Gtk.Label(label="Login Shell")
        self.shell = Gtk.Entry()
        self.shell.set_text("/bin/bash")

        sudoerLabel = Gtk.Label(label="Sudoer")
        self.sudoer = Gtk.CheckButton()

        groupsLabel = Gtk.Label(label="Default Groups")
        self.groups = Gtk.CheckButton()
        self.groups.set_active(True)

        userBtn = Gtk.Button(label="Add User")
        userBtn.connect("clicked", self.makeUser)

        closeBtn = Gtk.Button(label="Close")
        closeBtn.connect("clicked", self.close)

        grid.attach(title, 0, 0, 1, 1)
        grid.attach(nameLabel, 0, 1, 2, 1)
        grid.attach(self.name, 3, 1, 3, 1)
        grid.attach(passLabel, 0, 2, 2, 1)
        grid.attach(self.password, 3, 2, 3, 1)
        grid.attach(shellLabel, 0, 3, 2, 1)
        grid.attach(self.shell, 3, 3, 3, 1)
        grid.attach(groupsLabel, 0, 4, 2, 1)
        grid.attach(self.groups, 3, 4, 1, 1)
        grid.attach(sudoerLabel, 0, 5, 2, 1)
        grid.attach(self.sudoer, 3, 5, 1, 1)
        grid.attach(userBtn, 3, 7, 3, 2)
        grid.attach(closeBtn, 0, 7, 3, 2)

    def makeUser(self, widget):
        user = self.name.get_text()
        secret = self.password.get_text()
        shell = self.shell.get_text()
        sudoer = self.sudoer.get_active()
        groups = self.groups.get_active()
        subprocess.run(["./scripts/adduser", user, secret, "{}".format(sudoer), "{}".format(groups), shell])
    def close(self, widget):
        self.destroy()

class mainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Main Page")

        self.set_default_size(430, 230)
        self.set_border_width(5)
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_resizable(False)

        grid = Gtk.Grid(row_spacing = 10, column_spacing = 10, column_homogeneous = True)
        self.add(grid)

        title = Gtk.Label(label="<span size='x-large'>Welcome to OP Control Panel</span>")
        title.set_use_markup(True)

        desc = Gtk.Label(label="<span size='large'>OP Control Panel is simple 'control panel' app for Arch Linux</span>")
        desc.set_use_markup(True)

        button1 = Gtk.Button(label="Enable op-arch-repo")
        button1.connect("clicked", self.enable_repo)

        button2 = Gtk.Button(label="Enable Parallel Downloads")
        button2.connect("clicked", self.enable_parallel_downloads)

        button3 = Gtk.Button(label="Clear All Pacman and AUR Caches")
        button3.connect("clicked", self.clean_caches)

        button4 = Gtk.Button(label="Check out my Gitlab")
        button4.connect("clicked", self.open_gitlab)

        button5 = Gtk.Button(label="Add User")
        button5.connect("clicked", self.addUser)

        button6 = Gtk.Button(label="Clear ~/.cache/")
        button6.connect("clicked", self.clear_home_cache)

        button7 = Gtk.Button(label="Install AUR Package")
        button7.connect("clicked", self.install_aur_package)

        button8 = Gtk.Button(label="Delete User")
        button8.connect("clicked", self.delete_user)

        closeBtn = Gtk.Button(label="Close")
        closeBtn.connect("clicked", self.close)

        grid.attach(title, 0,0,9,2)
        grid.attach_next_to(desc, title, Gtk.PositionType.BOTTOM, 9, 5)
        grid.attach_next_to(button1, desc, Gtk.PositionType.BOTTOM, 3, 5)
        grid.attach_next_to(button8, button1, Gtk.PositionType.RIGHT, 2, 5)
        grid.attach_next_to(button2, button8, Gtk.PositionType.RIGHT, 4, 5)
        grid.attach_next_to(button3, button1, Gtk.PositionType.BOTTOM, 4, 5)
        grid.attach_next_to(button4, button3, Gtk.PositionType.RIGHT, 3, 5)
        grid.attach_next_to(button5, button4, Gtk.PositionType.RIGHT, 2, 5)
        grid.attach_next_to(button6, button3, Gtk.PositionType.BOTTOM, 3, 5)
        grid.attach_next_to(button7, button6, Gtk.PositionType.RIGHT, 4, 5)
        grid.attach_next_to(closeBtn, button7, Gtk.PositionType.RIGHT, 2, 5)

    def enable_repo(self, widget):
        subprocess.run(["./scripts/enablerepo"])
    def enable_parallel_downloads(self, widget):
        subprocess.run(["./scripts/enableparalleldownloads"])
    def clean_caches(self, widget):
        subprocess.run(["./scripts/clearcaches"])
    def close(self, widget):
        Gtk.main_quit()
    def open_gitlab(self, widget):
        subprocess.Popen(["xdg-open", "https://gitlab.com/orasponka"])
    def clear_home_cache(self, widget):
        subprocess.run(["./scripts/clearhomecache"])
    def addUser(self, widget):
        newWindow = userAddWindow()
        newWindow.show_all()
    def install_aur_package(self, widget):
        newWindow = aurInstallWindow()
        newWindow.show_all()
    def delete_user(self, widget):
        newWindow = deleteUserWindow()
        newWindow.show_all()

window = mainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
