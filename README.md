# OP Control Panel
OP Control Panel is simple python gtk gui app.
This is done with practice in mind. <br>
OP Control Panel works only on Arch Linux and Arch Linux based distros. <br><br>
![](https://gitlab.com/orasponka/op-control-panel/-/raw/main/screenshot-mainmenu.png)
## Requirments
You must have **python**, **gtk3**, some **polkit** agent, **alacritty** and **pgi** python module installed on your system. <br>
You must run Arch Linux or Arch Linux based distro otherwise shell scripts won't work.
## How to run OP Control Panel
First you need to install pgi if you have not already.
```
pip install pgi
or 
yay -S python-pgi
```
Then do things below
```
git clone https://gitlab.com/orasponka/op-control-panel.git
cd op-control-panel/
python app.py
```
## How to build OP Control Panel (Arch Linux)
```
git clone https://gitlab.com/orasponka/op-control-panel.git
cd op-control-panel/build/
makepkg
sudo pacman -U *.zst
```
## How to install OP Control Panel (Arch Linux)
If you have enabled my [op-arch-repo](https://gitlab.com/orasponka/op-arch-repo) you can install OP Control Panel via pacman.
```
sudo pacman -Sy op-control-panel
```
## Gallery
![](https://gitlab.com/orasponka/op-control-panel/-/raw/main/screenshot-all.png)
